# groff mom

My templates for groff in combination with mom

## Letter
Mom contains a subset of the document processing macros designed for ease of creation of correspondence.

1. Reference Macros
    * .AUTHOR "Name"
    * .DOCTYPE LETTER
    * .PRINTSTYLE TYPESET | TYPEWRITE
    * .START

2. Macros for entering the headers\
    * .DATE
    * .TO
    * .FROM
    * .GREETING

3. Writing the text 
    * .PP
    * .PP

4. Goodbye
    * .CLOSING

See for further reference 
https://www.complang.tuwien.ac.at/doc/groff/html/mom/letters.html
